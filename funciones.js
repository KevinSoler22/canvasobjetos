function inicializar(){

    let vo= document.getElementById('Vo');
    let wo= document.getElementById('Wo');
 
    let canvas = document.getElementById('canvas');
    let ctx = canvas.getContext('2d');
    

     ctx.save();
            ctx.beginPath();
            
            ctx.globalAlpha = 0.0;
            ctx.fillRect(10,450,30,20);
           
            ctx.translate(10, 450);

            ctx.rotate((Math.PI / 180) *(360-wo.value));
          
            ctx.translate(-10,-450);
            ctx.globalAlpha = 1.0;
            ctx.fillStyle = '#000000';
            ctx.fillRect(10,450,60,-30);
        
        ctx.restore();

        let ra=10;
        let t=0;
        let x=5;
        let y=445;

        let gra=9.8;

        let xo=x;
        let yo=y;

        let  tiem=(2*vo.value*(Math.sin((Math.PI/180)*(wo.value))))/gra;

        for(i=0;i<=tiem;i=i+0.5){
            x=xo+(vo.value*(Math.cos((Math.PI/180)*(360-wo.value)))*i);
            y=yo+(vo.value*(Math.sin((Math.PI/180)*(360-wo.value)))*i)+((gra)*(i*i))/2;

            ctx.beginPath();
            ctx.arc(x, y, ra, 0, Math.PI * 2, true);
            ctx.closePath();
            ctx.fillStyle = 'blue';
            ctx.fill();
            
     }

        }


